using System.Web.Mvc;
using System.Web.Routing;

namespace PollBox {
	public class RouteConfig {
		public static void RegisterRoutes(RouteCollection routes)
		{
			routes.Clear();
			routes.MapRoute(
				name: "Default",
				url: "",
				defaults: new { controller = "Poll",
				                action = "Index" }
			);
			routes.MapRoute(
				name: "{action}",
				url: "{action}/{id}",
				defaults: new { controller = "Poll",
				                action = "create",
				                id = UrlParameter.Optional
				              }
			);
        }
	}
}
