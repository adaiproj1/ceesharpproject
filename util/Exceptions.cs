using System;

namespace PollBox.Exceptions {
	//reject custom form request
	public class EInvalidForm : Exception { }
	//the poll doesn't not exist
	public class EPollNotFound : Exception { }
	//you have already voted on this poll
	public class EAlreadyVoted : Exception { }
	//the poll is disabled so you can't vote on it
	public class EPollDisabled : Exception { }
	// wrong poll or wrong key
	public class ECantDisable : Exception { }
	//missing information for poll creation
	public class EIncompletePoll : Exception {
		public EIncompletePoll(string msg) : base(msg) { }
	}
}
