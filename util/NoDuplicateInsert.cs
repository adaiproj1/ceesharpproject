using System.Collections.Generic;

namespace PollBox {
	static public partial class Util {
		/* put apart because it might become a performance
		 * bottleneck in the future
		 *
		 * used when building a list that should only have unique values
		 */
		static public void NoDuplicateInsert(List<string> list, string value)
		{
			if (!list.Exists((item) => item == value))
				list.Add(value);
		}
	}
}
