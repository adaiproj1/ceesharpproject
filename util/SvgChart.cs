﻿using System;
using System.Collections.Generic;
using PollBox.Models;

/* scalable charts
 *
 * check out http://www.w3.org/2000/svg
 * if you want to understand something
 */
namespace Svg {
	class Path {
		string value = "";
		/* all in absolute coordinates
		 */
		public void MoveTo(float x, float y)
		{
			value += string.Format(System.Globalization.CultureInfo.InvariantCulture,
			                       "M{0},{1} ", x, y);
		}
		public void Arc(float rx, float ry, float rotx, float angle, float destx, float desty)
		{
			bool sweep = true;
			bool largeArc = angle > (float)(Math.PI) || angle < -(float)(Math.PI);
			value += string.Format(System.Globalization.CultureInfo.InvariantCulture,
			                       "A{0},{1} {2} {3},{4} {5},{6} ",
			                       rx, ry, rotx, largeArc?1:0, sweep?1:0, destx, desty);
		}
		public void LineTo(float destx, float desty)
		{
			value += string.Format(System.Globalization.CultureInfo.InvariantCulture,
			                       "L{0},{1} ", destx, desty);
		}
		public string Close()
		{
			return value + "Z";
		}
	}
	public class Chart {
		const int INNER_CIRCLE_RAD = 3;
		const float LINE_WIDTH = 0.01f;
		const float CANVAS_SIZE = 1 + LINE_WIDTH;
		static XML.Element GenSlice(ResultsChoice value, float total, ref float offset)
		{
			float proportion = (float)(value.NbPicked) / total;
			float angle = proportion * (float)(Math.PI * 2);

			XML.Element slice = new XML.Element("path");
			XML.Element title = new XML.Element("title");
			title.raw = string.Format("{0} : {1} {2:##.##}%", value.Name,
			                          value.NbPicked, proportion * 100.0f);
			slice.children.Add(title);
			slice.attributes.Add("id", "p" + value.Id);
			slice.attributes.Add("fill", value.color);
			slice.attributes.Add("stroke", value.color);
			slice.attributes.Add("stroke-width", LINE_WIDTH.ToString());

			float srcx = 1+(float)Math.Cos(offset);
			float srcy = 1+(float)Math.Sin(offset);
			float destx = 1+(float)Math.Cos(offset+angle);
			float desty = 1+(float)Math.Sin(offset+angle);

			Path path = new Path();
			path.MoveTo(100, 100);
			path.LineTo(srcx * 100f, srcy * 100f);
			path.Arc(100, 100, 0, angle, destx * 100f, desty * 100f);
			slice.attributes.Add("d", path.Close());

			offset += angle;
			return slice;
		}
		static XML.Element GenImage(IReadOnlyList<ResultsChoice> values, string cssClass)
		{
			XML.Element root = new XML.Element("svg");

			root.attributes.Add("xmlns", "http://www.w3.org/2000/svg");
			root.attributes.Add("version", "1.1");
			root.attributes.Add("viewBox", "0 0 200 200");
			root.attributes.Add("class", cssClass);

			int total = 0;
			foreach (var value in values)
				total += value.NbPicked;

			float totalf = (float)total;

			if (total == 1)
				totalf += 0.001f;

			float offset = 0;
			foreach (var value in values)
				root.children.Add(GenSlice(value, totalf, ref offset));

			return root;
		}

		static public string Gen(IReadOnlyList<ResultsChoice> values, string cssClass)
		{
			XML.StringWriterUTF8 xmlStream = new XML.StringWriterUTF8();

			XML.Element image = GenImage(values, cssClass);
			image.Write(xmlStream);

			return xmlStream.ToString();
		}
	}
}
