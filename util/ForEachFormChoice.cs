using System;
using System.Collections.Generic;
using System.Collections.Specialized;

namespace PollBox {
	static public partial class Util {
		/* loops over a form's choices
		 *
		 * in our design, html forms contain 'name's
		 * that are ints   name="45"   name="2"
		 *
		 * this function allows us to loop over thoses
		 * IEnumarable was not used because
		 * KeyValuePair<int, string> kval; kval.key  kval.value
		 * looks aweful on the caller compared to (id, name) => { id     name}
		 */
		public static void
			ForEachFormChoice(NameValueCollection form, Action<int, string> action)
		{
			foreach (string key in form.AllKeys) {
				int id;
				if (int.TryParse(key, out id)) {
					action(id, form[key]);
				}
			}
		}
	}
}
