﻿using System;

namespace PollBox {
	static class RandColor {
		static byte[] last;
		static RandColor()
		{
			last = new byte[] {0x00, 0xFF, 0x00 };
		}
		public static string New() {
			last[0] = (byte)((last[0] + 0x27) % 0xFF);
			last[1] = (byte)((last[0] - 0x33) % 0xFF);
			last[2] = (byte)((last[0] + 0x77) % 0xFF);
			return "#" + HexStr.FromBytes(last);
		}
	}
}