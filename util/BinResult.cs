﻿using System.Web;
using System.Web.Mvc;

namespace PollBox
{
	/* arbitrary binary result
	 * compatible with the 'ActionResult' model of MVC
	 */
	class BinResult : FileResult
	{
		byte[] buf;
		public BinResult(byte[] buffer) : base("application/octet-stream")
		{
			buf = buffer;
		}
		protected override void WriteFile(HttpResponseBase response)
		{
			response.BinaryWrite(buf);
		}
	}
}
