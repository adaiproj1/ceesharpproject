﻿using System;
using System.Diagnostics.Contracts;

/* converts byte[] into string using hexadecimal representation
 * and back
 * there are tons of ways to do this
 * this implementation doesn't and wont do weird undocument things
 * it only assumes code points for '0' to '9' and for 'a' to 'f'
 * are sequential in the string representation
 */
namespace PollBox
{
	static class HexStr
	{
		/* unit test :  //TODO: automate unit test
		 * byte[] arr1 = { 0x09, 0x5F, 0xA2};
		 * string str = FromBytes(arr1);
		 * byte[] arr2 = ToBytes(str);
		 * if (cmpBytes(arr1, arr2) && str == "095fa2") //pass
		 */
		static readonly char[] byteToChar = {'0', '1', '2', '3', '4', '5', '6',
				'7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
		public static string FromBytes(byte[] arr)
		{
			string ret = "";
			for (int b = 0; b < arr.Length; b++) {
				//upper 4 bits
				ret += byteToChar[arr[b] >> 4];
				//lower 4 bits
				ret += byteToChar[arr[b] & 0xF];
			}

			return ret;
		}
		static byte ByteOfChar(char c)
		{
			if ('0' <= c && c <= '9')
				return (byte)(c - '0');
			else if ('a' <= c && c <= 'f')
				return (byte)(c - 'a' + 10);
			else
				throw new InvalidCastException("not an hexchar");
		}
		public static byte[] ToBytes(string str)
		{
			Contract.Ensures(Contract.Result<byte[]>().Length
							 == str.Length / 2);

			if (str.Length % 2 != 0)
				throw new InvalidCastException("uneven hexstr length");

			byte[] ret = new byte[str.Length / 2];

			for (int c = 0, b = 0; c < str.Length; c += 2, b++) {
				//upper 4 bits
				ret[b] = ByteOfChar(str[c + 1]);
				//lower 4 bits
				ret[b] |= (byte)(ByteOfChar(str[c]) << 4);
			}

			return ret;
		}
	}
}
