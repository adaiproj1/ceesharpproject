using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

/* dead simple no surprises XML generator
 * It doesn't support XML namespaces or any weird thing.
 * just nested key/values
 *
 * Make sure the graph is a tree.
 */
namespace XML {
	sealed class StringWriterUTF8 : StringWriter {
		public override Encoding Encoding
		{
			get { return Encoding.UTF8; }
		}
	}
	public class ExceptionInvalidXMLName : Exception {
		string msg;
		public override string Message { get { return msg; } }
		public ExceptionInvalidXMLName(string name)
		{
			msg = name + " is not a valid XML name";
		}
	}
	struct Element {
		string name;
		public List<Element> children;
		public Dictionary<string, object> attributes;
		public string raw; //can be null
		public Element(string name)
		{
			this.children = new List<Element>();
			this.attributes = new Dictionary<string, object>();
			validateName(name);
			this.name = name;
			this.raw = null;
		}
		static void validateName(string name)
		{
			foreach (char c in name)
				if (!char.IsLetter(c) && c != '_' && c != '-')
					throw new ExceptionInvalidXMLName(name);
		}
		static string FmtAttrValue(string attributeValue)
		{
			return attributeValue.Replace("\"", "&quot;");
		}
		public void Write(StringWriterUTF8 output)
		{
			output.Write("<{0}", this.name);

			foreach (var key in this.attributes.Keys) {
				validateName(key);
				string val = FmtAttrValue(attributes[key].ToString());
				output.Write(" {0}=\"{1}\"", key, val);
			}

			if (children.Count == 0 && raw == null) {
				output.Write("/>\n");
			} else {
				output.Write(">\n");

				foreach (var child in children)
					child.Write(output);

				if (raw != null)
					output.WriteLine(raw);

				output.Write("</{0}>\n", name);
			}
		}
	}
	class Document {
		public Element root;
		public void Write(StringWriterUTF8 output)
		{
			output.Write(@"<?xml version=""1.0"" encoding=""UTF-8""?>" + "\n");
			root.Write(output);
		}
	}
}
