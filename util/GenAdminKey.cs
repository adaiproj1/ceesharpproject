/* real random for generated passwords
 */
using System.Diagnostics.Contracts;
using System.Security.Cryptography;

namespace PollBox {
	static class AdminKey {
		const uint KEY_SIZE = 128;
		static RNGCryptoServiceProvider rand;
		static AdminKey()
		{
			rand = new RNGCryptoServiceProvider();
		}

		static public byte[] Generate()
		{
			Contract.Ensures(Contract.Result<string>().Length == KEY_SIZE);

			byte[] buf = new byte[KEY_SIZE];
			rand.GetBytes(buf);
			return buf;
		}
	}
}
