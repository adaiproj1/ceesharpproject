using System.Collections.Generic;

namespace PollBox.Models
{
	/// <summary>
	/// Poll taken from DB to display in page vote
	/// </summary>
	public class VotePoll
	{
		public int IdPoll { get; }
		public string Question { get; }
		public bool Multichoices { get; }
		public List<VoteChoice> Choices { get; }
		public VotePoll(string question, List<VoteChoice> choices, int idPoll, bool multichoices)
		{
			Question = question;
			Choices = choices;
			IdPoll = idPoll;
			Multichoices = multichoices;
		}
	}
}