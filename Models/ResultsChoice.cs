namespace PollBox.Models
{
	/// <summary>
	/// Choice list to display in page results
	/// </summary>
	public class ResultsChoice
	{
		public int Id { get; }
		public string Name { get; }
		public int NbPicked { get; }
		public string color { get; }
		public ResultsChoice(int id, string name, int nbPicked)
		{
			this.Id = id;
			this.Name = name;
			this.NbPicked = nbPicked;
			this.color = RandColor.New();
		}
	}
}