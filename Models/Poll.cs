using System.Collections.Generic;
using System.Collections.Specialized;

using PollBox.Exceptions;


namespace PollBox.Models {

	 /// <summary>
	 /// Poll taken from form
	 /// </summary>
	public class Poll {
		public string Question { get; }
		public List<string> Choices { get; }
		public bool IsMultiChoice { get; }
		public bool NeedsVoteDedup { get; }
		public Poll(string question, bool canPickMultiple,
					bool needsVoteDedup, NameValueCollection form)
		{
			if (question == null)
				throw new EInvalidForm();

			//the value isn't in the form when false
			this.IsMultiChoice = canPickMultiple;
			this.NeedsVoteDedup = needsVoteDedup;

			if (!SanatizeInputString(ref question))
				throw new EIncompletePoll("Il manque la question.");

			this.Question = question;

			this.Choices = ChoicesFromForm(form);
			if (this.Choices.Count < 2)
				throw new EIncompletePoll("Il faut au moins deux choix.");
		}

		static List<string> ChoicesFromForm(NameValueCollection form)
		{
			List<string> ret = new List<string>();
			Util.ForEachFormChoice(form, (id, name) => {
				if (SanatizeInputString(ref name))
					Util.NoDuplicateInsert(ret, name);
			});

			return ret;
		}
		/* takes an arbitrary choice string
		 * and change it so we don't have issues with it
		 * later on
		 * returns false if all the string content was removed
		 */
		static bool SanatizeInputString(ref string str)
		{
			//TODO: make a decision for html tags inside input strings
			str = str.Trim();
			return str != "";
		}
	}
}
