using System.Collections.Generic;

namespace PollBox.Models
{
	/// <summary>
	/// Completed poll to display in page results
	/// </summary>
	public class ResultsPoll
	{
		public string Question { get; }
		public List<ResultsChoice> Choices { get; }
		public int NbVoter { get; }
		public ResultsPoll(string question, List<ResultsChoice> choices, int nbVoter)
		{
			this.Question = question;
			this.Choices = choices;
			this.NbVoter = nbVoter;
		}
	}
}