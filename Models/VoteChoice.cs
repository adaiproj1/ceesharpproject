﻿namespace PollBox.Models
{
	/// <summary>
	/// Choice taken from DB to display in the list of choices in page vote
	/// </summary>
	public class VoteChoice
	{
		public string Name { get; }
		public int Id { get; }
		public VoteChoice(string name, int id)
		{
			Name = name;
			Id = id;
		}
	}
}
