namespace PollBox.Models
{
	/// <summary>
	/// Class contains idPoll and adminKey for security issue
	/// </summary>
	public class Credentials
	{
		public int Id { get; }
		public string Key { get; }
		public Credentials(int id, string key)
		{
			this.Id = id;
			this.Key = key;
		}
	}
}
