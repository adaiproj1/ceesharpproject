using System;
using System.Collections.Generic;
using System.Data.SqlClient;

using PollBox.Exceptions;
using PollBox.Models;

namespace PollBox {
	/// <summary>
	/// F
	/// </summary>
	partial class DAL
	{
		const string SqlConnectionString = @"Server=.;Database=pollDB; Trusted_Connection=Yes";
		
		#region result
		/// <summary>
		/// get poll for the results page
		/// </summary>
		/// <param name="id">idPoll</param>
		/// <returns></returns>
		public static ResultsPoll TryTakePoll(int id)
		{
			using (SqlConnection connect = new SqlConnection(SqlConnectionString))
			{
				connect.Open();
				SqlTransaction transac = connect.BeginTransaction();

				string DBquestion;
				int DBnbVoter;
				List<ResultsChoice> DBchoices;
				try
				{
					string cmd_string = @"SELECT question, nbVoter
					                      FROM poll
					                      WHERE id = @toto";
					using (SqlCommand cmd = new SqlCommand(cmd_string, connect, transac)) {
						cmd.Parameters.AddWithValue("@toto", id);
						using (SqlDataReader reader = cmd.ExecuteReader()) {
							if (!reader.Read())
								throw new EPollNotFound();

							DBquestion = (string)reader["question"];
							DBnbVoter = (int)reader["nbVoter"];
						}
					}
					DBchoices = TryTakeChoices(connect, transac, id);
					transac.Commit();
				}
				catch(Exception)
				{
					transac.Rollback();
					throw;
				}

				return new ResultsPoll(DBquestion, DBchoices, DBnbVoter); ;
			}
		}
		/// <summary>
		/// get the choice list of the poll
		/// </summary>
		/// <param name="connect"></param>
		/// <param name="transac"></param>
		/// <param name="id">idPoll</param>
		/// <returns></returns>
		static List<ResultsChoice> TryTakeChoices(SqlConnection connect, SqlTransaction transac, int id)
		{
			string cmd_string = @"SELECT id, name, nPicks
			                      FROM choice
			                      WHERE idPoll = @toto
			                      ORDER BY nPicks DESC";

			List<ResultsChoice> listChoices;
			using (SqlCommand cmd = new SqlCommand(cmd_string, connect, transac)) {
				cmd.Parameters.AddWithValue("@toto", id);

				listChoices = new List<ResultsChoice>();
				using (SqlDataReader reader = cmd.ExecuteReader()) {
					while (reader.Read()) {
						int idChoice = (int)reader["id"];
						string name = (string)reader["name"];
						int nbPicked = (int)reader["nPicks"];

						listChoices.Add(new ResultsChoice(idChoice, name, nbPicked));
					}
				}
			}
			return listChoices;
		}
		static void AddResult(List<byte> array, int idChoice, int nPicks)
		{
			/* serialize a result
			 * TODO: this is endianess dependant
			 */
			array.AddRange(BitConverter.GetBytes(idChoice));
			array.AddRange(BitConverter.GetBytes(nPicks));
		}
		public static byte[] GetResults(int id)
		{
			List<byte> ret = new List<byte>();

			string cmd_string = @"SELECT id, nPicks
			                      FROM choice
			                      WHERE idPoll = @idPoll";
			string cmd_string2 = @"SELECT nbVoter
			                      FROM poll
			                      WHERE id = @idPoll";
			using (var co = new SqlConnection(SqlConnectionString))
			{
				co.Open();
				SqlTransaction transac = co.BeginTransaction();
				using (SqlCommand cmd = new SqlCommand(cmd_string, co, transac)) {
					cmd.Parameters.AddWithValue("@idPoll", id);
					using (SqlDataReader recordSet = cmd.ExecuteReader()) {
						if (!recordSet.HasRows)
							throw new EPollNotFound();

						while (recordSet.Read())
							AddResult(ret, (int)recordSet[0], (int)recordSet[1]);
					}
				}
				using (SqlCommand cmd = new SqlCommand(cmd_string2, co, transac)) {
					cmd.Parameters.AddWithValue("@idPoll", id);
					AddResult(ret, -1, (int)cmd.ExecuteScalar());
				}
			}
			return ret.ToArray();
		}
		#endregion

		public static VotePoll GetModelVote(int idPoll)
		{
			using (SqlConnection connect = new SqlConnection(SqlConnectionString))
			{
				connect.Open();
				SqlTransaction transac = connect.BeginTransaction();

				string cmd_question = @"SELECT question, canPickMultiple, enabled
				                        FROM Poll
				                        WHERE id=@idPoll";

				string question;
				bool multichoices;
				List<VoteChoice> choices;

				try
				{
					using (SqlCommand cmd = new SqlCommand(cmd_question, connect, transac)) {
						cmd.Parameters.AddWithValue("@idPoll", idPoll);
						using (SqlDataReader reader = cmd.ExecuteReader()) {
							if (!reader.Read())
								throw new EPollNotFound();

							if (!(bool)reader["enabled"])
								throw new EPollDisabled();

							question = (string)reader["question"];
							multichoices = (bool)reader["canPickMultiple"];
						}
					}
					choices = GetChoices(connect, transac, idPoll);
					transac.Commit();
				}
				catch
				{
					transac.Rollback();
					throw;
				}

				return new VotePoll(question, choices, idPoll, multichoices);
			}
		}
		static List<VoteChoice> GetChoices(SqlConnection connect, SqlTransaction transac, int idPoll)
		{
			string cmd_choices = @"SELECT id, name
			                       FROM choice
			                       WHERE idPoll = @idPoll";

			List<VoteChoice> myChoices;
			using (SqlCommand cmd = new SqlCommand(cmd_choices, connect, transac)) {
				cmd.Parameters.AddWithValue("@idPoll", idPoll);

				using (SqlDataReader reader = cmd.ExecuteReader()) {
					myChoices = new List<VoteChoice>();

					while (reader.Read()) {
						int id = (int)reader["id"];
						string name = (string)reader["name"];

						myChoices.Add(new VoteChoice(name, id));
					}
				}
			}
			return myChoices;
		}

		public static bool didVote(string idVoter, int idPoll)
		{
			string cmd_string = @"SELECT COUNT(*)
			                      FROM voter
			                      WHERE idPoll = @idPoll
			                            AND id = @idVoter";
			using (SqlConnection co = new SqlConnection(SqlConnectionString)) {
				co.Open();
				using (SqlCommand cmd = new SqlCommand(cmd_string, co)) {
					cmd.Parameters.AddWithValue("@idVoter", idVoter);
					cmd.Parameters.AddWithValue("@idPoll", idPoll);
					return (int)(cmd.ExecuteScalar()) == 1;
				}
			}
		}

		static bool didVote(SqlConnection co, SqlTransaction transac,
		                           string idVoter, int idPoll)
		{
			string cmd_string = @"SELECT COUNT(*)
			                      FROM voter
			                      WHERE idPoll = @idPoll
			                            AND id = @idVoter";
			using (SqlCommand cmd = new SqlCommand(cmd_string, co, transac)) {
				cmd.Parameters.AddWithValue("@idVoter", idVoter);
				cmd.Parameters.AddWithValue("@idPoll", idPoll);
				return (int)(cmd.ExecuteScalar()) == 1;
			}
		}

		static bool needsPreventDupVotes(SqlConnection co, SqlTransaction transac,
		                                 int idPoll)
		{
			string cmd_string = @"SELECT needVoteDedup
			                      FROM poll
			                      WHERE id = @idPoll";
			using (SqlCommand cmd = new SqlCommand(cmd_string, co, transac)) {
				cmd.Parameters.AddWithValue("@idPoll", idPoll);
				using (SqlDataReader recordset = cmd.ExecuteReader()) {
					if (!recordset.Read())
						throw new EPollNotFound();

					return (bool)recordset["needVoteDedup"];
				}
			}
		}
		static void RegiterVoterOnPoll(SqlConnection co, SqlTransaction transac,
		                               string idVoter, int idPoll)
		{
			string cmd_string = @"INSERT INTO voter
			                      (id, idPoll)
			                      Values (@idVoter, @idPoll)";
			using (SqlCommand cmd = new SqlCommand(cmd_string, co, transac)) {
				cmd.Parameters.AddWithValue("@idVoter", idVoter);
				cmd.Parameters.AddWithValue("@idPoll", idPoll);
				cmd.ExecuteNonQuery();
			}
		}

		/* the transaction model forces
		 * us to leave some of the logic here
		 */
		public static void AddVotes(string idVoter, List<int> choix, int idPoll)
		{
			string cmd_string = @"UPDATE choice
			                      SET nPicks = nPicks+1
			                      WHERE id=@c AND idPoll=@idPoll";

			string cmd_string2 = @"UPDATE poll
			                      SET nbVoter = nbVoter+1
			                      WHERE id=@idPoll";

			using (SqlConnection connect = new SqlConnection(SqlConnectionString)) {
				connect.Open();
				SqlTransaction transac = connect.BeginTransaction();
				try {
					if (didVote(connect, transac, idVoter, idPoll))
						throw new EAlreadyVoted();
					
					using (SqlCommand cmd = new SqlCommand(cmd_string, connect, transac)) {
						cmd.Parameters.Add("@c", System.Data.SqlDbType.Int);
						cmd.Parameters.AddWithValue("@idPoll", idPoll);
						foreach (int c in choix) {
							cmd.Parameters["@c"].Value = c;
							cmd.ExecuteNonQuery();
						}
					}

					using (SqlCommand cmd = new SqlCommand(cmd_string2, connect, transac)) {
						cmd.Parameters.AddWithValue("@idPoll", idPoll);
						cmd.ExecuteNonQuery();
					}

					if (needsPreventDupVotes(connect, transac, idPoll))
						RegiterVoterOnPoll(connect, transac, idVoter, idPoll);
					
					transac.Commit();
				} catch (Exception) {
					transac.Rollback();
					throw;
				}
			}
		}

		/// <summary>
		///
		/// </summary>
		/// <param name="poll">Poll to insert</param>
		/// <returns></returns>
		public static Credentials AddPoll(Poll poll)
		{
			byte[] key = AdminKey.Generate();

			string cmd_string = @"INSERT INTO poll
			                      (question, adminKey, canPickMultiple,
			                       needVoteDedup, enabled, nbVoter)
			                      VALUES (@q, @key, @mult, @dedup, 1, 0);
			                      SELECT CAST(SCOPE_IDENTITY() AS INT)";

			int id;
			using (SqlConnection connect = new SqlConnection(SqlConnectionString))
			{
				connect.Open();
				SqlTransaction transac = connect.BeginTransaction();

				try {
					using (SqlCommand cmd = new SqlCommand(cmd_string, connect, transac)) {
						cmd.Parameters.AddWithValue("@q", poll.Question);
						cmd.Parameters.AddWithValue("@mult", poll.IsMultiChoice);
						cmd.Parameters.AddWithValue("@key", key);
						cmd.Parameters.AddWithValue("@dedup", poll.NeedsVoteDedup);
						id = (int)cmd.ExecuteScalar();
					}

					AddChoices(connect, transac, id, poll.Choices);
					transac.Commit();
				}
				catch (Exception)
				{
					transac.Rollback(); //will fail if the connection was lost
					throw;
				}
			}

			string strKey = HexStr.FromBytes(key);
			return new Credentials(id, strKey);
		}
		static void AddChoices(SqlConnection connec, SqlTransaction transac,
		                       int idPoll, List<string> choices)
		{
			string cmd_string = @"INSERT INTO choice
			                      (id, idPoll, name, nPicks)
			                      VALUES (@id, @idPoll, @name, 0)";
			using (SqlCommand cmd = new SqlCommand(cmd_string, connec, transac)) {
				cmd.Parameters.AddWithValue("@idPoll", idPoll);
				cmd.Parameters.Add("@id", System.Data.SqlDbType.Int);
				cmd.Parameters.Add("@name", System.Data.SqlDbType.NChar);
				for (int i = 0; i < choices.Count; i++) {
					cmd.Parameters["@id"].Value = i;
					cmd.Parameters["@name"].Value = choices[i];
					cmd.ExecuteNonQuery();
				}
			}
		}

		/* there are races with this
		 * but we don't allow for editing 'canPickMultiple'
		 * and poll disabling already has latency anyways
		 */
		public static bool PollIsMultiChoice(int idPoll)
		{
			string cmd_string = @"SELECT canPickMultiple, enabled
			                      FROM poll
			                      WHERE id = @idPoll";
			using (SqlConnection co = new SqlConnection(SqlConnectionString)) {
				co.Open();
				using (SqlCommand cmd = new SqlCommand(cmd_string, co)) {
					cmd.Parameters.AddWithValue("@idPoll", idPoll);
					using (SqlDataReader recordset = cmd.ExecuteReader()) {
						if (!recordset.Read())
							throw new EPollNotFound();

						if (!(bool)recordset["enabled"])
							throw new EPollDisabled();

						return (bool)recordset["canPickMultiple"];
					}
				}
			}
		}

		#region deactivate Poll

		/// <summary>
		/// Verify the idPoll and adminKey before requiring the confirmation of deactivation
		/// </summary>
		/// <param name="id">idPoll</param>
		/// <param name="key">adminKey</param>
		/// <returns>1 or 0</returns>
		public static bool PollKeyIsValid(int id, string key)
		{
			string cmd_string = @"SELECT enabled
			                      FROM poll
			                      WHERE id = @tete
			                            AND adminKey=@toto";
			byte[] binKey = HexStr.ToBytes(key);
			
			using (SqlConnection connexion = new SqlConnection(SqlConnectionString)) {
				connexion.Open();
				using (SqlCommand cmd = new SqlCommand(cmd_string, connexion)) {
					cmd.Parameters.AddWithValue("@tete", id);
					cmd.Parameters.AddWithValue("@toto", binKey);
					using (SqlDataReader recordset = cmd.ExecuteReader()) {
						if (!recordset.Read())
							return false;

						if (!(bool)recordset["enabled"])
							throw new EPollDisabled();

						return true;
					}
				}
			}
		}

		/// <summary>
		/// Try to deactivate the poll
		/// </summary>
		/// <param name="id">idPoll</param>
		/// <param name="key">adminKey</param>
		/// <returns></returns>
		public static void DisablePoll(int id, string key)
		{
			string cmd_string = @"UPDATE poll
			                      SET enabled = 'false'
			                      WHERE id = @toto
			                            AND adminKey=@tutu";
			byte[] binKey = HexStr.ToBytes(key);

			using (SqlConnection connexion = new SqlConnection(SqlConnectionString)) {
				connexion.Open();
				using (SqlCommand cmd = new SqlCommand(cmd_string, connexion)) {
					cmd.Parameters.AddWithValue("@toto", id);
					cmd.Parameters.AddWithValue("@tutu", binKey);

					if (cmd.ExecuteNonQuery() != 1)
						throw new ECantDisable();
				}
			}
		}
		#endregion
	}
}