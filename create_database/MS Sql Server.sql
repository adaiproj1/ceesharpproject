-- this only works with Ms Sql Server,
-- you will need to port it to use an other DBMS

CREATE DATABASE pollDB;
GO
USE pollDB;
CREATE TABLE poll (
	id INT NOT NULL IDENTITY(1, 1),
	question NVARCHAR(MAX) NOT NULL,
	adminKey BINARY(128) NOT NULL,
	canPickMultiple BIT NOT NULL,
	needVoteDedup BIT NOT NULL,
	enabled BIT NOT NULL,
	nbVoter INT NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE choice (
	id INT NOT NULL,
	idPoll INT NOT NULL,
	name NVARCHAR(MAX) NOT NULL,
	nPicks INT NOT NULL,
	PRIMARY KEY (id, idPoll)
);

CREATE TABLE voter (
	id NVARCHAR(50) NOT NULL,
	idPoll INT NOT NULL,
	PRIMARY KEY (id, idPoll)
);
