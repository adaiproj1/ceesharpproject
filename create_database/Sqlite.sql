-- only compatible with Sqlite
-- you will need to port it to use
-- it with an other DBMS

CREATE TABLE poll (
	id INTEGER PRIMARY KEY ASC,
	question NVARCHAR(500) NOT NULL,
	adminKey BINARY(128) NOT NULL,
	canPickMultiple BIT NOT NULL,
	needVoteDedup BIT NOT NULL,
	enabled BIT NOT NULL,
	nbVoter INTEGER NOT NULL
);

CREATE TABLE choice (
	id INTEGER NOT NULL,
	idPoll INTEGER NOT NULL,
	name NVARCHAR(500) NOT NULL,
	nPicks INTEGER NOT NULL,
	PRIMARY KEY (id, idPoll)
);

CREATE TABLE voter (
	id NVARCHAR(50) NOT NULL,
	idPoll INTEGER NOT NULL,
	PRIMARY KEY (id, idPoll)
);

