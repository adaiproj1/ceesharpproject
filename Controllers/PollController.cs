using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PollBox.Exceptions;
using PollBox.Models;

namespace PollBox.Controllers
{
	public class PollController : Controller
	{
		public ActionResult Index()
		{
			return View("Index");
		}

		#region Page Layout
		public ActionResult Manuel()
		{
			return View("Manuel");
		}
		public ActionResult Apropos()
		{
			return View("apropos");
		}
		public ActionResult Confidentialite()
		{
			return View("confidentialite");
		}
		#endregion

		/// <summary>
		/// Create the new poll
		/// </summary>
		/// <param name="nfields"></param>
		/// <returns></returns>
		[HttpGet]
		public ActionResult New(uint? nfields)
		{
			if (nfields == null)
				nfields = 2;

			return View("new", nfields.Value);
		}

		[HttpGet]
		public ActionResult Links(int? id, string key)
		{
			if (id == null || key == null)
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

			return View("Links", new Credentials(id.Value, key));
		}

		[HttpPost]
		public ActionResult Create(string question, bool? canPickMultiple,
								  bool? needsVoteDedup)
		{
			Poll pollToInsert;
			try {
				//form checkboxes are null when unticked
				pollToInsert = new Poll(question, canPickMultiple.HasValue,
				                        needsVoteDedup.HasValue, Request.Form);
			} catch (EIncompletePoll e) {
				return View("ErrorCreate", e);
			} catch (EInvalidForm) {
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}

			Credentials login;
			try {
				login = DAL.AddPoll(pollToInsert);
			} catch (Exception) {
#if DEBUG
				throw; // get the error when debugging
#else
				return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
#endif
			}
			/* I don't know if GET leaks the ley
			 */
			return Redirect("/Links/" + login.Id + "?key=" + login.Key);
		}

		static bool hasAlreadyVotedCookie(HttpCookieCollection cookies, int idPoll)
		{
			return cookies["CookieUser" + idPoll] != null;
		}

		[HttpGet]
		public ActionResult Vote(int? id)
		{
			if (id == null)
				return new HttpNotFoundResult();

			if (hasAlreadyVotedCookie(Request.Cookies, id.Value))
				return View("AlreadyVoted", id.Value);

			try {
				if (DAL.didVote(Request.UserHostAddress, id.Value))
					return View("AlreadyVoted", id);

				VotePoll model = DAL.GetModelVote(id.Value);
				return View("vote", model);
			} catch (EPollDisabled) {
				return View("PollDisabled", id);
			} catch (EPollNotFound) {
				return new HttpNotFoundResult();
			}
		}

		[HttpPost]
		public ActionResult AddVote(int? id, int? singleChoiceId)
		{
			if (id == null)
				return new HttpNotFoundResult();

			try {
				if (hasAlreadyVotedCookie(Request.Cookies, id.Value))
					throw new EAlreadyVoted();

				bool multichoice = DAL.PollIsMultiChoice(id.Value);

				List<int> choices = new List<int>();
				if (multichoice) {
					Util.ForEachFormChoice(Request.Form, (idC, name) => {
						choices.Add(idC);
					});
				} else if (singleChoiceId != null) {
					choices.Add(singleChoiceId.Value);
				} else {
					return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
				}

				DAL.AddVotes(Request.UserHostAddress, choices, id.Value);

				HttpCookie cookieWrite = new HttpCookie("CookieUser" + id);
				cookieWrite.Value = "";
				cookieWrite.Expires = DateTime.MaxValue;
				Response.Cookies.Add(cookieWrite);

				return Redirect("/results/" + id);
			} catch (EPollDisabled) {
				return View("PollDisabled", id);
			} catch (EAlreadyVoted) {
				return View("AlreadyVoted", id);
			} catch (EPollNotFound) {
				return new HttpNotFoundResult();
			} catch (Exception) {
#if DEBUG
				throw;
#else
				return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
#endif
			}
		}

#region results
		[HttpGet]
		/* for real time updates
		 * see Scripts/results/update.js
		 */
		public ActionResult UpdateResults(int? id)
		{
			if (id == null)
				return new HttpNotFoundResult();

			try {
				byte[] msg = DAL.GetResults(id.Value);
				BinResult ret = new BinResult(msg);
				return ret;
			} catch (EPollNotFound) {
				return new HttpNotFoundResult();
			} catch (Exception) {
#if DEBUG
				throw;
#else
				return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
#endif
			}
		}
		/// <summary>
		/// Display the results of a poll
		/// </summary>
		/// <param name="id">idPoll</param>
		/// <returns></returns>
		public ActionResult Results(int? id)
		{
			if (id == null)
				return new HttpNotFoundResult();

			try {
				//Envoie question, choix avec le nombre de votant du sondage
				ResultsPoll pollFull = DAL.TryTakePoll(id.Value);
				return View("Results", pollFull);
			} catch (EPollNotFound) {
				return new HttpNotFoundResult();
			} catch (Exception) {
#if DEBUG
				throw;
#else
				return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
#endif
			}
		}
#endregion

#region deactivate Poll
		/// <summary>
		/// Test the adminKey then request the confirmation of deactivation from creater
		/// </summary>
		/// <param name="id"></param>
		/// <param name="key"></param>
		/// <returns></returns>
		public ActionResult Deactivate(int? id, string key)
		{
			if (id == null)
				return new HttpNotFoundResult();
			try {
				if (DAL.PollKeyIsValid(id.Value, key)) {
					Credentials myLinkSecu = new Credentials(id.Value, key);
					return View("Deactivate", myLinkSecu);
				} else {
					return View("ErrorDeactivate");
				}
			} catch (EPollDisabled) {
				return View("DeactivateConfirmed");
			} catch (Exception) {
#if DEBUG
				throw;
#else
				return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
#endif
			}
		}

		/// <summary>
		/// Deactivate the poll
		/// </summary>
		/// <param name="id"></param>
		/// <param name="key"></param>
		/// <returns></returns>
		public ActionResult DeactivateConfirmed(int? id, string key)
		{
			if (id == null)
				return new HttpNotFoundResult();

			try {
				DAL.DisablePoll(id.Value, key);
				return View("DeactivateConfirmed");
			} catch (ECantDisable) {
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			} catch (Exception) {
#if DEBUG
				throw;
#else
				return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
#endif
			}
		}
#endregion
	}
}