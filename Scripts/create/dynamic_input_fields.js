"use strict";

//TODO: remove duplicates

//2 or more
const MIN_INPUT_FIELDS = 2;

/* removes empty input fields from `list`
 * leaves at least MIN_INPUT_FIELDS, the selected one
 * and the last one (for new inputs)
 */
function remove_unused_inputs(list, selected)
{
	var remains = list.children.length;

	//except the last one
	[].slice.call(list.children, 0, -1).every(li => {
		if (remains === MIN_INPUT_FIELDS)
			return false;

		//<li> -> <input>
		var input = li.childNodes[0];
		if (input.value === "" && input !== selected) {
			li.remove()
			remains--;
		}
		return true;
	});
}

function update_input_list(list, selected)
{
	/* add before so `remove_unused_inputs` can be
	 * aware of the new one
	 */
	if (list.lastElementChild.children[0] === selected)
		add_input(list);

	remove_unused_inputs(list, selected);
}

function input_on_change(kb_event)
{
	var selected = kb_event.target;

	//only triggered when the user starts editing, not during
	if (selected.value === "" && kb_event.code !== "Backspace") {
		//<ul> <- <li> <- <input>
		var list = selected.parentElement.parentElement;
		update_input_list(list, selected);
	}
}

/* creates a unique form name
 * assumes new fields are always inserted
 * at the last position and *after* calling
 * this function
 */
function get_next_form_name(list)
{
	//first one special case
	if (list.children.length === 0)
		return 0;

	// <ul> -> <li>
	var last_li = list.lastChild;
	// <li> -> <input>
	var input = last_li.children[0];
	return 1 + parseInt(input.name);
}

function add_input(list)
{
	var i = document.createElement('input');
	i.type = 'text';
	i.placeholder = 'Saisissez un choix';
	i.name = get_next_form_name(list);
	i.onkeypress = input_on_change;
	i.className = "new_input";
	/* Uncomment for the alternate input mode.
	 * There is an issue of the field moving away from the user's
	 * mouse on click.
	 */
	//i.onfocus = remove_unused_inputs;
	var li = document.createElement('li');
	li.appendChild(i);

	list.appendChild(li);
}

(function set_defaults(list, nfields)
{
	for (var i = 0; i < nfields; i++)
		add_input(list);
}) (inputs, MIN_INPUT_FIELDS);

