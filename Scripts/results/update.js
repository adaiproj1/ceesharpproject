"use strict";

const INTERVAL = 10000; //miliseconds

const POLL_ID = (function get_poll_id()
{
	return document.URL.split('/')[4];
})();

function gen_slice(proportion, total, offset)
{
	var angle = proportion * Math.PI * 2.0;
	var flag = (angle > Math.PI) || (angle < (-Math.PI));
	var srcx = 100.0 * (1+Math.cos(offset.value));
	var srcy = 100.0 * (1+Math.sin(offset.value));
	var destx = 100.0 * (1+Math.cos(offset.value+angle));
	var desty = 100.0 * (1+Math.sin(offset.value+angle));
	var d = "M" + 100 +","+ 100 + " ";
	d += "L"+ srcx +","+ srcy + " ";
	d += "A" + 100 +","+ 100 + " 0 " + (flag?1:0) + ",1 " + destx + "," + desty;
	d += " Z";
	offset.value += angle;
	return d;
}

function update_slice(id, npicks, total, offset)
{
	var path = document.getElementById("p"+id);
	var proportion = npicks / total;
	path.setAttribute("d", gen_slice(proportion, total, offset));

	//update the hint   'name : npicks proportion'
	var title = path.children[0];
	var prefix = title.innerHTML.split(":")[0];
	title.innerHTML = prefix + ": " + npicks + " "
		+ Number(proportion * 100.0).toFixed(2) + "%"; 
}

function npicks_from_record(tr)
{
	var td = tr.children[2];
	return parseInt(td.innerText);
}

function reorder_choices()
{
	[].slice.call(choices.children)
	.map(function (x) { return choices.removeChild(x); })
	.sort(function(tr1, tr2) {
		var v1 = npicks_from_record(tr1);
		var v2 = npicks_from_record(tr2);
		return v2 - v1;
	})
	.forEach(function (x) { choices.appendChild(x); });
}

function update()
{
	if (this.response == null)
		return;//TODO: report error ?

	var arr = new Int32Array(this.response);
	/* the format of the response is :
	 * struct { int32 id_choice; int32 npicks; }[]
	 */
	var total = 0;
	for (var i=0; i < arr.length-1; i+=2) {
		var id_choice = arr[i];
		var npicks = arr[i+1];

		if (id_choice == -1) //nbVoter
			nb_voter.innerText = npicks;

		var choice = document.getElementById(id_choice);
		if (choice == null)
			continue;

		total += npicks;
		choice.innerText = npicks;
	}

	var totalf = 0.0 + total;
	if (total == 1)
		totalf += 1.01;

	var offset = {value: 0.0};
	for (var i=0; i < arr.length-1; i+=2) {
		var id_choice = arr[i];
		var npicks = arr[i+1];
		update_slice(id_choice, npicks, total, offset);
	}

	reorder_choices()
}

function looped()
{
	var xhr = new XMLHttpRequest();
	xhr.open("get", "/UpdateResults/" + POLL_ID);
	xhr.responseType = "arraybuffer";
	xhr.onload = update;

	xhr.send(null);
}

(function init()
{
	setInterval(looped, INTERVAL);
})();
